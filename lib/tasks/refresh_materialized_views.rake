desc "Refreshes materialized views (Postgresql)"
task refresh_materialized_views: :environment do
  ActiveRecord::Base.connection.execute %{
    refresh materialized view concurrently customer_details
  }
end