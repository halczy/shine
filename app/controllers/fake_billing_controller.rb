class FakeBillingController < ApplicationController
  skip_before_action :authenticate_user!
  
  def show
    if params[:cardholder_id]
      render json: {
        cardNumber: Faker::Business.credit_card_number,
        cardType: Faker::Business.credit_card_type.upcase,
        expirationMonth: Faker::Business.credit_card_expiry_date.month,
        expirationYear:  Faker::Business.credit_card_expiry_date.year,
        detailsLink:     Faker::Internet.url
      }
    else
      head :not_found
    end
  end


end